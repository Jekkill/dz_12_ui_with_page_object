import chai from 'chai';
import { goto, run, stop } from "./lib/driver/browser";
import { steps } from './framework/step_objects';
import { fragments } from "./framework/page_fragments";

const { expect } = chai;

describe('Test suites for http://automationpractice.com site with Page Object', () => {

    let page;
    before(async () => {
        await run();
        page = await goto('http://automationpractice.com/index.php');
    });

    after(async () => {
        await stop();
    });

    it('Should successfully register', async () => {
        await steps().AuthStep().register(page);
        const userNameText = await fragments().TopMenuFragment().getUserName(page);
        expect(userNameText).to.have.string('Rick');
    });

    it('Should successfully logout', async () => {
        await steps().AuthStep().logout(page);
        const userIsLoggedIn = await fragments().TopMenuFragment().userIsLoggedIn(page);
        expect(userIsLoggedIn).to.equal(false);
    });

    it('Should successfully login', async () => {
        await steps().AuthStep().login(page);
        const userNameText = await fragments().TopMenuFragment().getUserName(page);
        expect(userNameText).to.have.string('Pavel Plyukhov');
    });

    it('Should add goods to carts', async () => {
        await goto('http://automationpractice.com/index.php?id_category=3&controller=category');
        await steps().CartStep().addToCart(page);
        // TODO: think about another way to wait sometime;
        await page.evaluate(() => {
            return new Promise((resolve) => setTimeout(resolve, 1000));
        });
        await page.goto('http://automationpractice.com/index.php?controller=order');
        const productCountInCartText = await fragments().TopMenuFragment().getProductCountInCart(page);
        expect(productCountInCartText).to.equal('1');
    });

    it('Should make order', async () => {
        const confirmOrderText = await steps().CartStep().orderProducts(page);
        expect(confirmOrderText).to.have.string('complete');
    });


});