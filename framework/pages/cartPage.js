const CartPage = function ()
{
    const moveToNextStepButton = '.cart_navigation  .button-medium';
    const agreeToTermsOfServiceCheckbox = '#cgv';
    const paymentButton = '.payment_module .bankwire';
    const confirmOrderButton = '#cart_navigation .button-medium';
    const successfullOrderBlock = '.box .cheque-indent';

    this.orderProducts = async function (page)
    {
        // Move to address;
        await page.click(moveToNextStepButton);
        // Move to shipping;
        await page.click(moveToNextStepButton);
        await page.click(agreeToTermsOfServiceCheckbox);
        // Move to payment;
        await page.click(moveToNextStepButton);
        await page.click(paymentButton);
        await page.click(confirmOrderButton);
        return page.textContent(successfullOrderBlock);
    };

};

export { CartPage };