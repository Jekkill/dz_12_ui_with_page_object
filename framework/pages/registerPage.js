import faker from 'faker';

const RegisterPage = function ()
{

    const genderButton = '#id_gender1';
    const customerFirstNameField = '#customer_firstname';
    const customerLastNameField = '#customer_lastname';
    const passwordField = '#passwd';
    const firstNameField = '#firstname';
    const lastNameField = '#lastname';
    const address1Field  = '#address1';
    const cityField = '#city';
    const stateField = '#id_state';
    const zipCodeField = '#postcode';
    const mobilePhoneField = '#phone_mobile';
    const registerButton = '#submitAccount';

    this.register = async function (page)
    {
        await page.click(genderButton);
        await page.fill(customerFirstNameField, 'Rick');
        await page.fill(customerLastNameField, faker.name.lastName());
        await page.fill(passwordField, faker.internet.password());
        await page.fill(firstNameField, faker.name.firstName());
        await page.fill(lastNameField, faker.name.lastName());
        await page.fill(address1Field, faker.address.streetName());
        await page.fill(cityField, faker.address.city());
        await page.selectOption(stateField, '7');
        await page.fill(zipCodeField, '06001');
        await page.fill(mobilePhoneField, faker.phone.phoneNumber('0165#######'));
        await page.evaluate(() => {
            const example = document.querySelector('#address_alias #alias');
            example.value = 'pluhov-pavel@rambler.ru';
        });
        await page.click(registerButton);
    };

};

export { RegisterPage };