const ProductPage = function ()
{

    const addToCartButton = '#add_to_cart .exclusive';
    this.addToCart = async function (page) {
        await page.click(addToCartButton);
    }

};

export { ProductPage };