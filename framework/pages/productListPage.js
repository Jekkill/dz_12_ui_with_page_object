const ProductListPage = function ()
{

    const productBlock = '.product_list li:first-child .img-responsive';

    this.gotoDetails = async function (page) {
        await page.click(productBlock, { position: { x: 10, y: 10 } });
    }

};

export { ProductListPage };