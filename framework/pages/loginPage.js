import faker  from 'faker';

const LoginPage = function ()
{
    const emailLoginField = '#login_form #email';
    const emailRegisterField = '#create-account_form #email_create';
    const passwordField = '#login_form #passwd';
    const loginButton = '#login_form #SubmitLogin';
    const registerButton = '#create-account_form #SubmitCreate';

    this.login = async function (page)
    {
        await page.click(emailLoginField);
        await page.fill(emailLoginField, 'pluhov-pavel@rambler.ru');
        await page.click(passwordField);
        await page.fill(passwordField, 'YSoQ9e4KugVqlFubthIY');
        await page.click(loginButton);
    };

    this.register = async function (page)
    {
        await page.click(emailRegisterField);
        await page.fill(emailRegisterField, faker.internet.email());
        await page.click(registerButton);
    }
};

export { LoginPage };