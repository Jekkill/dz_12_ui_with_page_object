import { LoginPage } from './loginPage';
import { RegisterPage } from "./registerPage";
import { ProductListPage } from "./productListPage";
import { ProductPage } from "./productPage";
import { CartPage } from "./cartPage";

const app = () => ({
   LoginPage: () => new LoginPage(),
   RegisterPage: () => new RegisterPage(),
   ProductListPage: () => new ProductListPage(),
   ProductPage: () => new ProductPage(),
   CartPage: () => new CartPage()
});

export { app };