import { TopMenuFragment } from "./topMenuFragment";

const fragments = () => ({
    TopMenuFragment: () => new TopMenuFragment(),
});

export { fragments };