const TopMenuFragment = function ()
{

    const loginButton = '#header .nav .header_user_info .login';
    const logoutButton = '#header .nav .header_user_info .logout';
    const userNameBlock = '#header .nav .header_user_info .account';
    const productCountInCartBlock = '#header .shopping_cart';

    this.gotoLoginPage = async function (page)
    {
        await page.click(loginButton);
    };

    this.getUserName = async function (page)
    {
        return await page.textContent(userNameBlock);
    };

    this.logout = async function (page)
    {
        await page.click(logoutButton);
    };

    this.userIsLoggedIn = async function (page)
    {
        const element = await page.$$(userNameBlock);
        return !!element.length;
    };

    this.getProductCountInCart = async function (page)
    {
        let productBlockText = await page.textContent(productCountInCartBlock);
        productBlockText = productBlockText.replace(/\n/g, '');
        productBlockText = productBlockText.replace(/\t/g, ' ');
        productBlockText = productBlockText.replace(/\s+/g, ' ');
        return productBlockText.split(' ')[2];
    }
};

export { TopMenuFragment };
