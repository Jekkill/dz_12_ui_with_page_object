import { app } from "../pages";
import { fragments } from '../page_fragments';

const AuthStepObject = function () {
    this.login = async function (page) {
        await fragments().TopMenuFragment().gotoLoginPage(page);
        await app().LoginPage().login(page);
    };

    this.register = async function (page) {
        await fragments().TopMenuFragment().gotoLoginPage(page);
        await app().LoginPage().register(page);
        await app().RegisterPage().register(page);
    };

    this.logout = async function (page) {
        await fragments().TopMenuFragment().logout(page);
    }

};

export { AuthStepObject };