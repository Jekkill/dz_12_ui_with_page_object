import { AuthStepObject } from "./auth";
import { CartStepObject } from "./cart";

const steps = () => ({
    AuthStep: () => new AuthStepObject(),
    CartStep: () => new CartStepObject()
});

export { steps };