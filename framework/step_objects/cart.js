import { app } from "../pages";

const CartStepObject = function () {

    this.addToCart = async function (page) {
        await app().ProductListPage().gotoDetails(page);
        await app().ProductPage().addToCart(page);
    };

    this.orderProducts = async function (page) {
        return await app().CartPage().orderProducts(page);
    };

};

export { CartStepObject };